'''
Created on Oct 14, 2019

@author: HEMEL
'''

import sys
import collections
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import QtCore
from PyQt5.QtCore import Qt

from PyQt5.QtGui import QIcon
from PyQt5.QtPrintSupport import QPrintDialog, QPrinter, QPrintPreviewDialog
from PyQt5.Qt import QFileInfo,QFont

#Data fatch unit
class DataFatch:
    def __init__(self,data_dir,file_name):
        self.data_dir=data_dir
        self.file_name=file_name
        
    def readtext(self):
        #data_dir = "../resources/train_data/"
        file_to_open = self.data_dir + self.file_name #specify file location
        
        try:
            f=(open(file_to_open,'r',encoding = 'utf-8'))
              
        except FileNotFoundError:
            print("No such file or directory")
            
        return f.read()
            
#         finally:
#             print("File path error")

            
#Generate frequency word list
class FrequencyCount:
    
    def __init__(self):
        pass
    def words(self,text):
        word_list=text.split() #generate word list
        #print(word_list)
        return word_list
    def train(self,features):
        model = collections.defaultdict(lambda: 1) #set default key value 1
        for f in features:
            model[f]+=1 #increment key value by 1
        #print(len(model))
        return model
    

#perform actual correction
class Correction:
    
    def __init__(self,alphabet):
        self.alphabet=alphabet
        
    #Train data location
    df=DataFatch("resources/train_data/","big.txt")
    fc=FrequencyCount()
    
    word_frequency_list = fc.train(fc.words(df.readtext()))
    
      
    def edits1(self,word):
        splits     = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        #print (splits)
        deletes    = [a + b[1:] for a, b in splits if b]
        #print (deletes)
        transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b)>1]
        #print (transposes)
        replaces   = [a + c + b[1:] for a, b in splits for c in self.alphabet if b]
        #print (replaces)
        inserts    = [a + c + b     for a, b in splits for c in self.alphabet]
        #print (inserts)
        ret = set(deletes + transposes + replaces + inserts)
        #print (len(ret))
        return ret
      
    def known_edits2(self,word):
        return set(e2 for e1 in self.edits1(word) for e2 in self.edits1(e1) if e2 in self.word_frequency_list)
      
    def known(self,words): return set(w for w in words if w in self.word_frequency_list)
      
    # return most possible correct word
    def correct(self,word):
        candidates = self.known([word]) or self.known(self.edits1(word)) or self.known_edits2(word) or [word]
        return max(candidates, key=self.word_frequency_list.get)



class Main(QMainWindow,QDialog):
    
    def __init__(self):
        super().__init__()
        self.title="EN Spell Corrector"
#         self.left=100
#         self.top=50
#         self.height=450
#         self.width=500
        self.top = 100
        self.left = 100
        self.width = 550
        self.height = 550
        self.setWindowIcon(QIcon("resources/icons/icon.png"))
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setFixedSize(550, 550)
                    
        self.in_label=QLabel(self)
        self.in_label.move(240,55)
        self.in_label.setText("Input Text")
        self.in_label.setFont(QFont("Times", 10, QFont.Bold))
        
        self.out_label=QLabel(self)
        self.out_label.move(235,280)
        self.out_label.setText("Correct Text")
        self.out_label.setFont(QFont("Times", 10, QFont.Bold))        
        
        self.createEditor()
        self.CreateMenu()
        self.CreateButton()
        self.show()
    
#     def initUI(self):
#         self.setWindowTitle(self.title)
#         self.setGeometry(self.left,self.top,self.width,self.height)
#         
#         
#         self.CreateMenu()
        
        # main Text-box
#        self.textbox = QTextEdit(self)
#         self.textbox.setPlainText("জমাট ব্যাটিং শুরু করেছিলেন কাল খেলার প্রথম দিনেই..\n")
#         self.textbox.move(20, 50)
#         self.textbox.resize(460,300)
        
        
        # Button
#         self.button = QPushButton('Start processing', self)
#         self.button.setToolTip('Click here to correct text')
#         self.button.move(220,355)
        # Button action handler
#         self.button.clicked.connect(self.on_click)

        
#         self.show()
    
    


    
    #Create Menu 
    def CreateMenu(self):
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('File')
        editMenu = mainMenu.addMenu('Edit')
        viewMenu = mainMenu.addMenu('View')
        helpMenu = mainMenu.addMenu('Help')
        printAction = QAction(QIcon("resources/icons/print.png"), "Print", self)
        printAction.triggered.connect(self.printDialog)
        fileMenu.addAction(printAction)
        printPreviewAction = QAction(QIcon("resources/icons/printprev.png"), "Print Preview", self)
        printPreviewAction.triggered.connect(self.printpreviewDialog)
        fileMenu.addAction(printPreviewAction)
        exportpdfAction = QAction(QIcon("resources/icons/pdf.png"), "Export PDF", self)
        exportpdfAction.triggered.connect(self.printPDF)
        fileMenu.addAction(exportpdfAction)
        exiteAction = QAction(QIcon("resources/icons/exit.png"), 'Exit', self)
        exiteAction.setShortcut("Ctrl+E")
        exiteAction.triggered.connect(self.exitWindow)
        fileMenu.addAction(exiteAction)
        copyAction = QAction(QIcon("resources/icons/copy.png"), 'Copy', self)
        copyAction.setShortcut("Ctrl+C")
        editMenu.addAction(copyAction)
        saveAction = QAction(QIcon("resources/icons/save.png"), 'Save', self)
        saveAction.setShortcut("Ctrl+S")
        editMenu.addAction(saveAction)
        pasteAction = QAction(QIcon("resources/icons/paste.png"), 'Paste', self)
        pasteAction.setShortcut("Ctrl+P")
        editMenu.addAction(pasteAction)
        fontAction = QAction(QIcon("resources/icons/font.png"), "Font", self)
        fontAction.setShortcut("Ctrl+F")
        fontAction.triggered.connect(self.fontDialog)
        viewMenu.addAction(fontAction)
        colorAction = QAction(QIcon("resources/icons/color.png"), "Color", self)
        colorAction.triggered.connect(self.colorDialog)
        viewMenu.addAction(colorAction)
        self.toolbar = self.addToolBar('Toolbar')
        self.toolbar.addAction(copyAction)
        self.toolbar.addAction(saveAction)
        self.toolbar.addAction(pasteAction)
        self.toolbar.addAction(fontAction)
        self.toolbar.addAction(colorAction)
        self.toolbar.addAction(printAction)
        self.toolbar.addAction(printPreviewAction)
        self.toolbar.addAction(exportpdfAction)
        self.toolbar.addAction(exiteAction)

    def exitWindow(self):
        self.close()
        

    def createEditor(self):
        self.textEdit = QTextEdit(self)
#         self.setCentralWidget(self.textEdit)
        self.textEdit.move(20, 80)
        self.textEdit.resize(510,200)
        
        #add font style option
        self.font = QFont()
        self.font.setPointSize(11)
#         self.font.setBold(True)
        
        # add font style to the text-box
        self.textEdit.setFont(self.font) 
        self.textEdit.setAlignment(Qt.AlignLeft|Qt.AlignTop)
        
        df=DataFatch("resources/input_data/","input.txt")
        input_text=df.readtext()
        self.textEdit.insertPlainText(input_text)
        
        
        #new text box
        self.out_textEdit = QTextEdit(self)
#         self.setCentralWidget(self.textEdit)
        self.out_textEdit.move(20, 305)
        self.out_textEdit.resize(510,200)
        
        #add font style option
        self.font = QFont()
        self.font.setPointSize(11)
#         self.font.setBold(True)
        
        # add font style to the text-box
        self.out_textEdit.setFont(self.font) 
        self.out_textEdit.setAlignment(Qt.AlignLeft|Qt.AlignTop)        
        
        

    def CreateButton(self):
        self.button = QPushButton('Start processing', self)
        self.button.setToolTip('Click here to correct text')
        self.button.resize(250,35)
        self.button.move(155,510)
        
        #set button font style
        self.button_font = QFont()
        self.button_font.setPointSize(11)
#         self.button_font.setBold(True)
        
        self.button.setFont(self.button_font)
        
        self.button.clicked.connect(self.on_click)
        
    #button click action 
    def on_click(self):
        #bangla alphabet list
        bn_alphabet="abcdefghijklmnopqrtuvwxyzABCDEFGHIJKLMNOPQRST."
        #Test data location
        df=DataFatch("resources/input_data/","input.txt") 
        output=open("resources/output_data/output.txt","w")
        
        #create and object of Correction calls
        corr=Correction(bn_alphabet)
         
        input_text= df.readtext()
        #self.textEdit.insertPlainText("Hello")
         
#         correct_text=''
        for word in input_text.split():
            #print (f'{corr.correct(word)} ')
            self.out_textEdit.insertPlainText(corr.correct(word)+' ')
#             correct_text+=f'{corr.correct(word)}'
            
#         self.textbox.insertPlainText(correct_text)
             
#         print(correct_text)
        
        #output.write(self.textEdit)
        
    def fontDialog(self):
        font, ok = QFontDialog.getFont()
        if ok:
            self.textEdit.setFont(font)

    def colorDialog(self):
        color = QColorDialog.getColor()
        self.textEdit.setTextColor(color)

    def printDialog(self):
        printer = QPrinter(QPrinter.HighResolution)
        dialog = QPrintDialog(printer, self)
        if dialog.exec_() == QPrintDialog.Accepted:
            self.textEdit.print_(printer)
            
    def printpreviewDialog(self):
        printer = QPrinter(QPrinter.HighResolution)
        previewDialog = QPrintPreviewDialog(printer, self)
        previewDialog.paintRequested.connect(self.printPreview)
        previewDialog.exec_()

    def printPreview(self, printer):
        self.textEdit.print_(printer)

    def printPDF(self):
        fn, _ = QFileDialog.getSaveFileName(self, 'Export PDF', None, 'PDF files (.pdf);;All Files()')
        if fn != '':
            if QFileInfo(fn).suffix() == "" : fn += '.pdf'
            printer = QPrinter(QPrinter.HighResolution)
            printer.setOutputFormat(QPrinter.PdfFormat)
            printer.setOutputFileName(fn)
            self.textEdit.document().print_(printer)
        
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    main= Main()
#     main.show()
    sys.exit(app.exec_())
